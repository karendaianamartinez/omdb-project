import { useEffect, useState } from "react";
import { Login, Home, Register } from "./features";

function App() {
  const [user, setUser] = useState(null);

  const [loading, setLoading] = useState(true);

  const credentials = sessionStorage.getItem("credentials");

  const handleUserLogin = (credentials: any) => {
    setUser(JSON.parse(credentials));
    // setUser(JSON.parse(credentials));
    setLoading(!loading);
  };

  useEffect(() => {
    if (user === null) handleUserLogin(credentials);
  }, [credentials]);

  if(loading) return <p>Is loading</p>;

  return <div className="App">{user ? <Home /> : <Login />}</div>;
}

export default App;
