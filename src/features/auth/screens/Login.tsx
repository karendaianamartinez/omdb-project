import { useState } from "react";

import { useForm } from "react-hook-form";

export interface UserLogin {
  username: string;
  password: string;
}

const Login = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<UserLogin>();

  const onSubmit = handleSubmit((data) => {
    const objAsString = JSON.stringify(data);

    sessionStorage.setItem("credentials", objAsString);
    console.log(data);
  });

  return (
    <div>
      <p>Login screen</p>

      <form onSubmit={onSubmit}>
        <div>
          <label>Username</label>
          <input
            aria-label="username"
            type="text"
            {...register("username", {
              required: true,
              pattern: /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
            })}
            placeholder="youremail@gmail.com"
          />
          {errors?.username && <p>Invalid email</p>}
        </div>

        <div>
          <label>password</label>
          <input aria-label="password" type="password" {...register("password", { required: true })} />
          {errors?.password && <p>Password is required.</p>}
        </div>

        <input type="submit" value="Submit" />
      </form>
    </div>
  );
}

export { Login };