import { render, screen, fireEvent } from "@testing-library/react"; // (or /dom, /vue, ...)

import { Login } from "../screens";

describe("Login", () => {
  test("should renders correctly", () => {
    render(<Login />);

    const paragraphTitle = screen.getByText(/login/i);

    const labelUsername =  screen.getByLabelText(/username/i, { 
      selector: "input"
    })
    const inputUsername = screen.getByRole("textbox", { name: "username" });
    const placeholderUsername = screen.getByPlaceholderText("youremail@gmail.com");

    const labelPassword = screen.getByLabelText(/password/i);
    // const inputPassword = screen.getByRole("textbox", { name: "password" });

    const buttonElement = screen.getByRole("button", {
      name: "Submit",
    });
  
    expect(paragraphTitle).toBeInTheDocument();

    expect(labelUsername).toBeInTheDocument();
    expect(inputUsername).toBeInTheDocument();
    expect(placeholderUsername).toBeInTheDocument();

    expect(labelPassword).toBeInTheDocument();
    // expect(inputPassword).toBeInTheDocument();

    expect(buttonElement).toBeInTheDocument();


  });

})

