import { render, screen } from "@testing-library/react"; // (or /dom, /vue, ...)
import { Home } from "../screens";


describe("Home test", () => {
  test("Home component renders correctly", () => {
    render(<Home />);

    const textEelment = screen.getByText(/home/i);

    expect(textEelment).toBeInTheDocument();
    // const linkElement = screen.getByText(/learn react/i);
    // expect(linkElement).toBeInTheDocument();
  });

})

