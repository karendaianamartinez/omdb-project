module.exports = {
  collectCoverage: true,
  collectCoverageFrom: ["src/features/**/*.{js,jsx}"],
};
